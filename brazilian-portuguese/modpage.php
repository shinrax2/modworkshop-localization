<?php

$l['mydownloads_being_updated'] = 'Atualizando';
$l['mydownloads_meta_by'] = '{1} por {2}'; //Mod X by Y;
$l['mydownloads_cannot_rate_own'] = 'Você não pode avaliar os seus própios mods.';
$l['mydownloads_collaborators_desc'] = 'Os Colaboradores são as pessoas que você dá a permissão de editar o mod. Eles não podem excluir o seu mod, transferir o direito de dono ou editar os colaboradores.';
$l['mydownloads_comment_banned'] = "Você não pode comentar enquanto você estiver banido.";
$l['mydownloads_delete_confirm'] = "Tem certeza que você quer excluir esse comentário?";
$l['mydownloads_download_description'] = "Descrição";
$l['mydownloads_download_is_suspended'] = '
Esse mod foi suspenso e só está visível para o seu autor e os moderadores do site.
<br/>A suspensão é tempoária por investigação ou permanentemente devido a violação das <a style="text-decoration:underline;" href="/rules">regras</a>.
<br/>Você deve contactar os moderadores sobre isso ou se o seu mod foi atualizado para seguir as regras, você pode mandar uma "Aplicação sem Uso" <a style="text-decoration:underline;"  href="/forumdisplay.php?fid=66">aqui</a>.';
$l['mydownloads_download_changelog'] = 'Notas de Mudanças';
$l['mydownloads_download'] = "Baixar";
$l['mydownloads_license'] = 'Licensa';
$l['mydownloads_report_download'] = 'Denunciar o Mod';
$l['show_download_link_warn'] = 'Tome cuidado de links suspeitos. Se você achar que o link é malicioso, por favor denuncie o mod';
$l['show_download_link'] = 'Mostrar o link de download';
$l['show_files'] = 'Mostrar os arquivos';
$l['submitted_by'] = 'Enviado por';
$l['subscribe_commnet_help'] = 'Seja notificado quando alguém comenta nessa sessão de comentários';
$l['follow_mod_help'] = 'Segue esse mod para mostrar nos seus mods seguidos e seja notificado quando o mod tem uma atualização';
$l['mydownloads_download_comments'] = "Comentários";
$l['mydownloads_unsuspend_it'] = 'Sem suspensão';
$l['mydownloads_suspend_it'] = 'Com suspensão';
$l['mydownloads_files_alert'] = '<strong>SEM ARQUIVOS</strong> | Desde que o mod está sem arquivos, o mod vai está como invisível.';
$l['mydownloads_files_alert_waiting'] = '<strong>SEM ARQUIVOS APROVADOS</strong> | Esse mod está atualmente invisível até que o(s) arquivo(s) que você enviou seja(m) aprovado(s).';
